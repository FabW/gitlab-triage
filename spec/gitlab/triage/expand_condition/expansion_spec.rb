require 'spec_helper'

require 'gitlab/triage/expand_condition/expansion'

describe Gitlab::Triage::ExpandCondition::Expansion do
  subject do
    described_class.new(/\{(\d+)\.\.(\d+)\}/) do |(lower, upper)|
      Integer(lower)..Integer(upper)
    end
  end

  describe '#perform' do
    it 'expands string from sequences and returns the products of all' do
      strings = subject.perform(%w[missed:{0..1} missed:{2..3} missed])

      expect(strings).to eq(
        [
          %w[missed:0 missed:2 missed],
          %w[missed:0 missed:3 missed],
          %w[missed:1 missed:2 missed],
          %w[missed:1 missed:3 missed]
        ]
      )
    end
  end

  describe '#product_of_all' do
    it 'returns the product of list of lists' do
      strings = subject.product_of_all([%w[a:0 a:1], %w[b:2 b:3], %w[c]])

      expect(strings).to eq(
        [
          %w[a:0 b:2 c],
          %w[a:0 b:3 c],
          %w[a:1 b:2 c],
          %w[a:1 b:3 c]
        ]
      )
    end
  end

  describe '#expand_patterns' do
    it 'expands string from patterns' do
      strings = subject.expand_patterns('missed:{0..1}.{2..3}')

      expect(strings).to eq(
        %w[
          missed:0.2
          missed:0.3
          missed:1.2
          missed:1.3
        ]
      )
    end
  end

  describe '#scan_patterns' do
    it 'scans and produces sequences' do
      sequences = subject.scan_patterns('missed:{10..15}.{0..15}')

      expect(sequences).to eq([10..15, 0..15])
    end
  end
end
